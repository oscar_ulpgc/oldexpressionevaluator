package evaluator;

import org.junit.Assert;
import org.junit.Test;

public class ExpressionEvaluatorTest {       
    
    private final double delta = 0.01;
    
    @Test
    public void testConstantExpressions() {      
        Assert.assertEquals(1, new Constant(1).calculate());
        Assert.assertEquals(8.0, (double) new Constant(8.0).calculate(), delta);
    }
    
    @Test
    public void testAdditionTypes() {
        Assert.assertEquals(2, (int) new Addition(new Constant(1), new Constant(1)).calculate());
        Assert.assertEquals(2.5, (double) new Addition(new Constant(1), new Constant(1.5)).calculate(), delta);
        Assert.assertEquals(2.5, (double) new Addition(new Constant(1.5), new Constant(1)).calculate(), delta);
        Assert.assertEquals(5, (double) new Addition(new Constant(2.5), new Constant(2.5)).calculate(), delta);
    }
    
    @Test
    public void testSubtractionTypes() {
        Assert.assertEquals(3, (int) new Subtraction(new Constant(5), new Constant(2)).calculate());
        Assert.assertEquals(3.2, (double) new Subtraction(new Constant(4), new Constant(0.8)).calculate(), delta);
        Assert.assertEquals(5.1, (double) new Subtraction(new Constant(7.1), new Constant(2)).calculate(), delta);
        Assert.assertEquals(2, (double) new Subtraction(new Constant(4.5), new Constant(2.5)).calculate(), delta);
    }
    
    @Test
    public void testMultiplicationTypes() {
        Assert.assertEquals(10, (int) new Multiplication(new Constant(5), new Constant(2)).calculate());
        Assert.assertEquals(2.8, (double) new Multiplication(new Constant(4), new Constant(0.7)).calculate(), delta);
        Assert.assertEquals(14.2, (double) new Multiplication(new Constant(7.1), new Constant(2)).calculate(), delta);
        Assert.assertEquals(11.25, (double) new Multiplication(new Constant(4.5), new Constant(2.5)).calculate(), delta);
    }
    
    @Test
    public void testDivisionTypes() {
        Assert.assertEquals(2, (int) new Division(new Constant(5), new Constant(2)).calculate(), delta);
        Assert.assertEquals(5.71, (double) new Division(new Constant(4), new Constant(0.7)).calculate(), delta);
        Assert.assertEquals(3.55, (double) new Division(new Constant(7.1), new Constant(2)).calculate(), delta);
        Assert.assertEquals(1.8, (double) new Division(new Constant(4.5), new Constant(2.5)).calculate(), delta);
    }
}