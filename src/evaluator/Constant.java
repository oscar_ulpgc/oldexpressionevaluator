package evaluator;

public class Constant <Type> implements Expression {
    
    Type value;

    public Constant(Type value) {
        this.value = value;
    }

    @Override
    public Type calculate() {
        return value;
    }
    
}
