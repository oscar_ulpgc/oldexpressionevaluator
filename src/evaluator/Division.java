package evaluator;

public class Division <Type> extends BinaryOperation {

    public Division(Expression left, Expression right) {
        super(left, right);
    }    
}
