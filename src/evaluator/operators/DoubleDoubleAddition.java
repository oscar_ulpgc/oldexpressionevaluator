package evaluator.operators;

public class DoubleDoubleAddition extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left + (Double) right;
    }
    
}