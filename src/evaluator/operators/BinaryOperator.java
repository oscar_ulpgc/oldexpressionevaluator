package evaluator.operators;

public abstract class BinaryOperator {
    
    public abstract Object calculate(Object left, Object right);
}
