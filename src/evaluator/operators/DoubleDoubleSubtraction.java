package evaluator.operators;

/**
 *
 * @author oscar
 */
public class DoubleDoubleSubtraction extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left - (Double) right;
    }
    
}
