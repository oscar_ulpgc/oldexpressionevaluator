package evaluator.operators;

public class IntegerIntegerMultiplication extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left * (Integer) right;
    }
    
}
