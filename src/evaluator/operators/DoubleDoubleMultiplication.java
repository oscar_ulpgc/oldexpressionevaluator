package evaluator.operators;

public class DoubleDoubleMultiplication extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left * (Double) right;
    }
    
}
