package evaluator.operators;

/**
 *
 * @author oscar
 */
public class DoubleIntegerAddition extends BinaryOperator {    

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left + (Integer) right;
    }
    
}
