
package evaluator.operators;

public class DoubleIntegerDivision extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left / (Integer) right;
    }

}
