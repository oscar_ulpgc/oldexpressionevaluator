package evaluator.operators;

public class IntegerIntegerDivision extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left / (Integer) right;
    }
    
}
