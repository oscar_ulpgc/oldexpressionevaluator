
package evaluator.operators;

public class DoubleDoubleDivision extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left / (Double) right;
    }
    
}
