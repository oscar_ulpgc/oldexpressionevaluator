package evaluator.operators;

public class DoubleIntegerMultiplication extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left * (Integer) right;
    }
    
}
