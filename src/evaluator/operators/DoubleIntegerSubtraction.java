package evaluator.operators;

/**
 *
 * @author oscar
 */
public class DoubleIntegerSubtraction extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left - (Integer) right;
    }
    
}
