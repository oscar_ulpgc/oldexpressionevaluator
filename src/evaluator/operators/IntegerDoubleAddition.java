package evaluator.operators;

/**
 *
 * @author oscar
 */
public class IntegerDoubleAddition extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left + (Double) right; 
    }
    
}
