package evaluator.operators;

/**
 *
 * @author oscar
 */
public class IntegerIntegerSubtraction extends BinaryOperator {

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left - (Integer) right;
    }
    
}
