package evaluator;

public class Multiplication <Type> extends BinaryOperation {

    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }
}
