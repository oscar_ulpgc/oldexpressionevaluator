package evaluator;

public class Subtraction <Type> extends BinaryOperation {
    
    public Subtraction(Expression left, Expression right) {
        super(left, right);
    }   

}
