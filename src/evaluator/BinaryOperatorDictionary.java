package evaluator;

import evaluator.operators.DoubleDoubleDivision;
import evaluator.operators.DoubleIntegerDivision;
import evaluator.operators.IntegerDoubleDivision;
import evaluator.operators.IntegerIntegerDivision;
import evaluator.operators.DoubleDoubleAddition;
import evaluator.operators.DoubleDoubleSubtraction;
import evaluator.operators.DoubleIntegerAddition;
import evaluator.operators.DoubleIntegerSubtraction;
import evaluator.operators.IntegerDoubleAddition;
import evaluator.operators.IntegerDoubleSubtraction;
import evaluator.operators.IntegerIntegerAddition;
import evaluator.operators.IntegerIntegerSubtraction;
import evaluator.operators.BinaryOperator;
import evaluator.operators.DoubleDoubleMultiplication;
import evaluator.operators.DoubleIntegerMultiplication;
import evaluator.operators.IntegerDoubleMultiplication;
import evaluator.operators.IntegerIntegerMultiplication;
import java.util.HashMap;
import java.util.Set;
import org.reflections.Reflections;

public class BinaryOperatorDictionary {

    private HashMap<String, BinaryOperator> operators;
    
    public BinaryOperatorDictionary() {
        this.operators = new HashMap<>();
        addOperators();
    }
    
    public BinaryOperator getOperator(String operator, Object left, Object right) {
        return operators.get(getSignature(operator, left, right));
    }   
    
    private String getSignature(String operator, Object left, Object right) {
        return left.getClass().getSimpleName() + right.getClass().getSimpleName() + operator;
    }
   
    private void addOperators() {
        addAdditionOperators();
        addSubtractionOperators();
        addMultiplicationOperators();
        addDivisionOperators();
    }
    
    private void addAdditionOperators() {
        operators.put("IntegerIntegerAddition", new IntegerIntegerAddition());
        operators.put("IntegerDoubleAddition", new IntegerDoubleAddition());
        operators.put("DoubleIntegerAddition", new DoubleIntegerAddition());
        operators.put("DoubleDoubleAddition", new DoubleDoubleAddition());
    }

    private void addSubtractionOperators() {
        operators.put("IntegerIntegerSubtraction", new IntegerIntegerSubtraction());
        operators.put("IntegerDoubleSubtraction", new IntegerDoubleSubtraction());
        operators.put("DoubleIntegerSubtraction", new DoubleIntegerSubtraction());
        operators.put("DoubleDoubleSubtraction", new DoubleDoubleSubtraction());
    }
    
    private void addMultiplicationOperators() {
        operators.put("IntegerIntegerMultiplication", new IntegerIntegerMultiplication());
        operators.put("IntegerDoubleMultiplication", new IntegerDoubleMultiplication());
        operators.put("DoubleIntegerMultiplication", new DoubleIntegerMultiplication());
        operators.put("DoubleDoubleMultiplication", new DoubleDoubleMultiplication());
    }
    
    private void addDivisionOperators() {
        operators.put("IntegerIntegerDivision", new IntegerIntegerDivision());
        operators.put("IntegerDoubleDivision", new IntegerDoubleDivision());
        operators.put("DoubleIntegerDivision", new DoubleIntegerDivision());
        operators.put("DoubleDoubleDivision", new DoubleDoubleDivision());
    }

    private Set<Class<? extends BinaryOperator>> getBinaryOperators() {
        Reflections reflections = new Reflections("evalutators.operators");
        Set<Class<? extends BinaryOperator>> set = reflections.getSubTypesOf(BinaryOperator.class);        
        return set;
    }
}
