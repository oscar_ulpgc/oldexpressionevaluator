package evaluator;

import evaluator.operators.BinaryOperator;

public abstract class BinaryOperation implements Expression {
    protected final Expression left;
    protected final Expression right;

    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }    
    
    @Override
    public Object calculate() {
        return calculate(left.calculate(), right.calculate());
    }
    
    private Object calculate(Object left, Object right) {
        return createBinaryOperator(left, right).calculate(left, right);
    }
    
    public BinaryOperator createBinaryOperator(Object left, Object right) {
        return new BinaryOperatorDictionary().getOperator(this.getClass().getSimpleName(), left, right);
    }
}
