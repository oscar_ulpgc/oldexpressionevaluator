package evaluator;

public class Addition <Type> extends BinaryOperation {

    public Addition(Expression left, Expression right) {
        super(left, right);
    }   
}
